#!/bin/sh
if [ -z $STOW_DIR ]
then
    echo "error: STOW_DIR is unset or empty" >&2
    exit 1
fi

for FILE in "$@"
do
    if [ $(find $@ -type l | wc -l) -gt 0 ]
    then
	echo "Symlinks found in source tree! This is not supported by stow. Aborting."
	exit 1
    fi
done

DATE=$(date +"%d-%m-%y_%H-%M-%S")
TARGET_PATH="$STOW_DIR/stowed_$DATE"

mkdir -p $TARGET_PATH

for FILE in "$@"
do
    # full absolute path:
    filepath=`readlink -f $FILE`

    # removing the filename (even if it's a dir as it will be copied):
    filepath=`echo $filepath | sed 's#/[^/]*$##'`
    # removing '/home/': 
    filepath=`echo $filepath | sed 's#/home/[^/]*##'`

    mkdir -p $TARGET_PATH/$filepath
    mv $FILE $TARGET_PATH/$filepath
done
