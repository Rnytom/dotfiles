(setq user-mail-address	"raphael.melotte@posteo.net"
      user-full-name	"Raphaël Mélotte")

;; resources:
;; https://github.com/redguardtoo/mastering-emacs-in-one-year-guide/blob/master/gnus-guide-en.org
;; https://www.emacswiki.org/emacs/GnusGmail

(setq gnus-select-method
      '(nnimap "gmail"
	       (nnimap-address "imap.gmail.com")
	       (nnimap-server-port "imaps")
	       (nnimap-stream ssl)
	       ;; @see http://www.gnu.org/software/emacs/manual/html_node/gnus/Expiring-Mail.html
               ;; press 'E' to expire email
               (nnmail-expiry-target "nnimap+gmail:[Gmail]/Corbeille")
               (nnmail-expiry-wait immediate)
	       (nnir-search-engine imap)))

(setq message-send-mail-function 'smtpmail-send-it
      smtpmail-smtp-server "smtp.gmail.com"
      smtpmail-smtp-service 587
      ;; Make Gnus NOT ignore [Gmail] mailboxes
      ;; @see https://www.emacswiki.org/emacs/GnusGmail
      gnus-ignored-newsgroups "^to\\.\\|^[0-9. ]+\\( \\|$\\)\\|^[\"]\"[#'()]")

;(setq gnus-summary-display-arrow t)
;(setq gnus-summary-line-format "%U%R%z%B%(%[%4L: %-10,10f%]%) %d %s\n")

;; from:
;; https://www.emacswiki.org/emacs/GnusFormatting
(setq-default
 gnus-summary-line-format "%U%R%z %(%&user-date;  %-15,15f  %B%s%)\n"
 gnus-user-date-format-alist '((t . "%Y-%m-%d %H:%M"))
; gnus-summary-thread-gathering-function 'gnus-gather-threads-by-references
 gnus-sum-thread-tree-false-root ""
 gnus-sum-thread-tree-indent " "
 gnus-sum-thread-tree-leaf-with-other "├► "
 gnus-sum-thread-tree-root ""
 gnus-sum-thread-tree-single-leaf "╰► "
 gnus-sum-thread-tree-vertical "│")

(setq gnus-article-date-headers '(user-defined)
      gnus-article-time-format
      (lambda (time)
        (let* ((date (format-time-string "%a, %d %b %Y %T %z" time))
               (local (article-make-date-line date 'local))
               (combined-lapsed (article-make-date-line date
                                                        'combined-lapsed))
               (lapsed (progn
                         (string-match " (.+" combined-lapsed)
                         (match-string 0 combined-lapsed))))
          (concat local lapsed))))

;; @see https://stackoverflow.com/a/12129376
(setq gnus-thread-sort-functions
      '(gnus-thread-sort-by-number
        gnus-thread-sort-by-date))
