(menu-bar-mode -1)
(toggle-scroll-bar -1)
(tool-bar-mode -1)
(setq inhibit-startup-screen t)

;; font size:
(set-face-attribute 'default nil :height 90)

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#212526" "#ff4b4b" "#b4fa70" "#fce94f" "#729fcf" "#e090d7" "#8cc4ff" "#eeeeec"])
 '(custom-enabled-themes (quote (tango-dark)))
 ;; open directories with sunrise-commander
 '(find-directory-functions (quote (sr-dired cvs-dired-noselect dired-noselect)))
 '(org-agenda-files (quote ("~/Syncthing/mobile-share/org/")))
 '(org-default-notes-file "~/Syncthing/mobile-share/org/notes.org")
 '(org-refile-targets
   (quote
    ((nil :maxlevel . 1)
     (org-agenda-files :maxlevel . 1))))
 '(org-support-shift-select t)
 '(send-mail-function (quote mailclient-send-it))
 '(xterm-mouse-mode t))

(setq org-refile-use-outline-path 'file)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-switchb)

;; also see http://sachachua.com/blog/2015/02/learn-take-notes-efficiently-org-mode/
(global-set-key (kbd "C-c o")
                (lambda () (interactive) (find-file "~/Syncthing/mobile-share/org/notes.org")))



(require 'magit)
(global-set-key (kbd "C-x g") 'magit-status)

(setq comint-prompt-read-only t)


;; dired
;; if another dired buffer is open, try to guess the copy/move target from that
(setq dired-dwim-target t)

(add-to-list 'load-path "~/.emacs.d/lisp/")


;; sunrise-commander
(load "sunrise-commander") ;; best not to include the ending “.el” or “.elc”
(load "sunrise-x-buttons")
(require 'sunrise-commander)
(require 'sunrise-x-buttons)

;; allow virtual directories
(add-to-list 'auto-mode-alist '("\\.srvm\\'" . sr-virtual-mode))


;; mu4e configuration

(require 'mu4e)

;; use mu4e for e-mail in emacs
(setq mail-user-agent 'mu4e-user-agent)

(setq mu4e-maildir "~/Mail"
      mu4e-contexts
 `( ,(make-mu4e-context
     :name "Gmail"
     :match-func (lambda (msg) (when msg
				 (string-prefix-p "/gmail" (mu4e-message-field msg :maildir))))
     :vars '(( user-mail-address      . "raphael.melotte@gmail.com"  )
             ( user-full-name         . "Raphaël Mélotte" )
	     (smptmail-smtp-user . "raphael.melotte@gmail.com")
	     (smtpmail-smtp-server . "smtp.gmail.com")
	     (smtpmail-smtp-service . 587)
	     ;; don't save message to Sent Messages, Gmail/IMAP takes care of this
	     (mu4e-sent-messages-behaviour . 'delete)
	     (mu4e-trash-folder      . "/gmail/[Gmail]/Corbeille")
	     (mu4e-drafts-folder . "/gmail/[Gmail]/Brouillons")
	     (mu4e-sent-folder . "/gmail/[Gmail]/Messages envoy&AOk-s")))
    ,(make-mu4e-context
      :name "posteo"
      :match-func (lambda (msg) (when msg
       (string-prefix-p "/posteo" (mu4e-message-field msg :maildir))))
      :vars '(
	      ( user-mail-address      . "raphael.melotte@posteo.net"  )
              ( user-full-name         . "Raphaël Mélotte" )
	      (smptmail-smtp-user . "rnytom@posteo.net")
	      (smtpmail-smtp-server . "posteo.de")
	      (smtpmail-smtp-service . 587)
	      (mu4e-trash-folder      . "/posteo/Trash")
	      (mu4e-drafts-folder . "/posteo/Drafts")
	      (mu4e-sent-folder . "/posteo/Sent")))))

;; setup some handy shortcuts
;; you can quickly switch to your Inbox -- press ``ji''
;; then, when you want archive some messages, move them to
;; the 'All Mail' folder by pressing ``ma''.

;(setq mu4e-maildir-shortcuts
;    '( ("/INBOX"               . ?i)
;       ("/[Gmail].Sent Mail"   . ?s)
;       ("/[Gmail].Trash"       . ?t)
;       ("/[Gmail].All Mail"    . ?a)))

;; allow for updating mail using 'U' in the main view:
(setq mu4e-get-mail-command "mbsync --config ~/.config/mbsync/mbsyncrc -a")

;; don't forget to fill in ~/.authinfo.gpg
(setq message-send-mail-function 'smtpmail-send-it
      send-mail-function 'smtpmail-send-it)

;; don't keep message buffers around
(setq message-kill-buffer-on-exit t)

(setq mu4e-use-fancy-chars t)
(setq mu4e-update-interval 300)

;; speed up indexing (do NOT modify the maildir externally while using this)
(setq
  mu4e-index-cleanup nil      ;; don't do a full cleanup check
  mu4e-index-lazy-check t)    ;; don't consider up-to-date dirs


;; mu4e-alert notifications
(mu4e-alert-set-default-style 'libnotify)
(add-hook 'after-init-hook #'mu4e-alert-enable-notifications)
(add-hook 'after-init-hook #'mu4e-alert-enable-mode-line-display)

;; emacs: always edit symlinks at their own location:
(setq vc-follow-symlinks t)

;; guix:
(global-set-key (kbd "C-x C-g") 'guix-packages-by-name)

;; tramp:
(setq tramp-default-method "ssh")

;; use M-ARROW to move to another window:
(windmove-default-keybindings 'meta)
(defun format-whole-buffer ()
  "format whole buffer"
  (interactive)
  (delete-trailing-whitespace)
  (indent-region (point-min) (point-max) nil)
  (untabify (point-min) (point-max)))

(global-set-key [f8] 'format-whole-buffer)

(add-to-list 'tramp-remote-path 'tramp-own-remote-path)
