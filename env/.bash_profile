[[ -f $HOME/.profile ]] && . .profile

if [ "$BASH" ]; then
  if [ -f ~/.bashrc ]; then
    . ~/.bashrc
  fi
fi

export PATH="$HOME/.config/guix/current/bin:$PATH"
export GUIX_LOCPATH="$HOME/.guix-profile/lib/locale"
export INFOPATH="$HOME/.config/guix/current/share/info:$INFOPATH"
GUIX_PROFILE="$HOME/.guix-profile" 
. $GUIX_PROFILE/etc/profile

export SSL_CERT_DIR="$HOME/.guix-profile/etc/ssl/certs"
export SSL_CERT_FILE="$HOME/.guix-profile/etc/ssl/certs/ca-certificates.crt"
export GIT_SSL_CAINFO="$SSL_CERT_FILE"
export CURL_CA_BUNDLE="$SSL_CERT_FILE"

export TERMINAL="xterm"

export STOW_DIR="$HOME/stow"
